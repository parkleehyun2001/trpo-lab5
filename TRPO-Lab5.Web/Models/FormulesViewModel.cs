﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRPO_Lab5.Web.Models
{
    public class FormulesViewModel
    {
        public double A{ get; set; }

        public double B{ get; set; }
         
        public double C{ get; set; }

        public double P{ get; set; }

        public double Square { get; set; }
    }
}
