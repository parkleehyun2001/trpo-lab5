﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static TRPO_Lab5.Web.Models.FormulesViewModel;
using static TRPO_Lab3.Lib.Formules;


namespace TRPO_Lab5.Web.Controllers
{
    public class FormulesController : Controller
    {
        [HttpGet]
        public ActionResult TreyGSq()
        {
            return View();
        }

        [HttpPost]
        public ActionResult TreyGSq(string a, string b, string c, string s)
        {
            double square = TreyG(Convert.ToDouble(a), Convert.ToDouble(b), Convert.ToDouble(c), Convert.ToDouble(s));
            ViewBag.Result = Math.Round(square, 3);
            return View();
        }

    }
}
