﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

let formInputs = document.querySelectorAll('.form__input');
let formButton = document.querySelector('.form__btn');
let formUnvalid = document.querySelector('.form__unvalid');

formUnvalid.classList.add("disabled");

for (let input of formInputs) {
    input.oninput = function () {
        if (input.value < 0) {
            formButton.disabled = true;
            formUnvalid.classList.remove("disabled");
        }
        else {
            formButton.disabled = false;
            formUnvalid.classList.add("disabled");
        }
    };