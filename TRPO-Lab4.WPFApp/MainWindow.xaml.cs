﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TRPO_Lab4.WPFApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = new Model();
        }
    }

    class Model: INotifyPropertyChanged
    {
        private double a;
        public double A
        {
            set
            {
                a = value;
                OnPropertyChanged(nameof(A));
                OnPropertyChanged(nameof(S));
            }
            get
            { return a; }
        }

        private double b;
        public double B
        {
            set
            {
                b = value;
                OnPropertyChanged(nameof(B));
                OnPropertyChanged(nameof(S));

            }
            get { return b; }
        }

        private double c;
        public double C
        {
            set
            {
                c = value;
                OnPropertyChanged(nameof(C));
                OnPropertyChanged(nameof(S));

            }
            get { return c; }
        }

        private double s;
        public double P
        {
            set
            {
                s = value;
                OnPropertyChanged(nameof(P));
                OnPropertyChanged(nameof(S));
            }
            get { return s; }
        }

        public double S
        {
            get
            {
                return Formules.TreyG(a, b, c, s);
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        
    }
}

