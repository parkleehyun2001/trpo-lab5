﻿using System;

namespace TRPO_Lab3.Lib
{
    public class Formules
    {
        public static double TreyG(double a, double b, double c, double s)
        {
            double TreyG = Math.Sqrt(s * (s - a) * (s - b) * (s - c));
            return TreyG;
        }
    }
}
